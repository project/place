Overview:
--------
The Place set of modules provides a way to use taxonomy to describe location, and tries where possible to use existing modules for all other location-related functionality.

This allows much more interaction with place as a concept than the single database table approach of location.module

Taxonomy_location module allows site administrators to associate locations
with taxonomy terms.

Features:
--------
 - 

Installation and configuration:
------------------------------
Please refer to the INSTALL file for complete installation and 
configuration instructions.

Comments:
--------
I want to try very hard to help all location-related modules play well with one another and share code and functionality when possible.  Contact me with ways I should change this module toward that end, and related modules to extend it, and in general how location modules could potentially and should hook into one another.

Requires:
--------
 - Drupal 5
 - taxonomy_location.module enabled (bundled in this package)
 - enabled taxonomy.module

Credits:
-------
 - Written and maintained by Benjamin Melan�on of Agaric Design Collective
   http://agaricdesign.com/