<?php
// $id


function place_ui_latlon_validate() {
  
}

function place_ui_latitude_validate() {
  
}

function place_ui_longitude_validate() {
  
}

/*

        if (user_access('submit latitude/longitude')) {
          if ((!strlen(trim($node->locations[$index]['latitude'])) && strlen(trim($node->locations[$index]['longitude']))) ||
              (strlen(trim($node->locations[$index]['latitude'])) && !strlen(trim($node->locations[$index]['longitude'])))) {
            form_set_error('locations]['. $index .'][latitude', t('You must fill out both longitude and latitude or you must leave them both blank.'));
            form_set_error('locations]['. $index .'][longitude', NULL);
          }
          elseif (strlen(trim($node->locations[$index]['latitude'])) && strlen(trim($node->locations[$index]['longitude']))) {
            if (!is_numeric($node->locations[$index]['latitude']) || $node->locations[$index]['latitude'] > 90.0 || $node->locations[$index]['latitude'] < -90.0) {
              form_set_error('locations]['. $index .'][latitude', t('Your latitude must be a numeric value between -90.0 and 90.0.'));
            }
            
            if (!is_numeric($node->locations[$index]['longitude']) || $node->locations[$index]['longitude'] > 180.0 || $node->locations[$index]['longitude'] < -180.0) {
              form_set_error('locations]['. $index .'][longitude', t('Your longitude must be a numeric value between -180.0 and 180.0.'));
            }
          }
        }



      foreach ($node->locations as $index => $location) {
        
        foreach (location_field_names() as $field_name => $display_name) {
          $workflow_setting = variable_get('location_'. $field_name .'_'. $node->type, $field_name == 'country' ? 1 : 0);
          // We only enforce required fields on the first location if there are multiple locations
          if ($index == 0) {
            if (variable_get('location_'. $field_name .'_'. $node->type, 0) == 2) {
              if (isset($node->locations[$index][$field_name]) && !strlen(trim($node->locations[$index][$field_name]))) {
                form_set_error('locations]['. $index .']['. $field_name, t('The field %field is required.', array('%field' => $display_name)));
              }
            }
          }
          
          $node->locations[$index][$field_name] = trim($node->locations[$index][$field_name]);
        }
        
        
        // Check if the (province, country) pair is valid
        if (isset($node->locations[$index]['province']) && !empty($node->locations[$index]['province']) && $node->locations[$index]['province'] != 'xx' &&
            isset($node->locations[$index]['country']) && !empty($node->locations[$index]['country']) && $node->locations[$index]['country'] != 'xx') {
          $province_list_function = 'location_province_list_'. $node->locations[$index]['country'];
          if (function_exists($province_list_function)) {
            $translated_location = location_form2api($node->locations[$index]);
            if (!in_array($translated_location['province'], array_keys($province_list_function()))) {
              form_set_error('locations]['. $index .'][province', t('Please make sure to select a state/province from the country you have selected.'));
            }
          }
        }
      
        // Check if submitted lat/lon are valid
        if (user_access('submit latitude/longitude')) {
          if ((!strlen(trim($node->locations[$index]['latitude'])) && strlen(trim($node->locations[$index]['longitude']))) ||
              (strlen(trim($node->locations[$index]['latitude'])) && !strlen(trim($node->locations[$index]['longitude'])))) {
            form_set_error('locations]['. $index .'][latitude', t('You must fill out both longitude and latitude or you must leave them both blank.'));
            form_set_error('locations]['. $index .'][longitude', NULL);
          }
          elseif (strlen(trim($node->locations[$index]['latitude'])) && strlen(trim($node->locations[$index]['longitude']))) {
            if (!is_numeric($node->locations[$index]['latitude']) || $node->locations[$index]['latitude'] > 90.0 || $node->locations[$index]['latitude'] < -90.0) {
              form_set_error('locations]['. $index .'][latitude', t('Your latitude must be a numeric value between -90.0 and 90.0.'));
            }
            
            if (!is_numeric($node->locations[$index]['longitude']) || $node->locations[$index]['longitude'] > 180.0 || $node->locations[$index]['longitude'] < -180.0) {
              form_set_error('locations]['. $index .'][longitude', t('Your longitude must be a numeric value between -180.0 and 180.0.'));
            }
          }
        }
      }
*/