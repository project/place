<?php

/**
 * @file
 * Autocomplete functions to accept a parent term
 */

/* We will use user module as our autocomplete module instead of taxonomy because we want *one term only* not multi/free tagging.   That, uh, only gets us so far...*/

/**
 * Retrieve a pipe delimited string of autocomplete suggestions for relevant terms
 *
 * This is very place_taxonomy specific:  Not only does it restrict the results
 * to a parent term, but it restricts to a weight (commandeered for level).
 *
 * See place_taxonomy_smartcomplete for a more taxonomy-general function, which
 * will be useful for getting any term restricted by parent and vocab
 */
function place_taxonomy_autocomplete($level, $parent, $string = '') {
// @TODO: check efficiency- should it be passed in, or is it cached for sure?
  $vid = place_taxonomy_get_vid();
  $matches = array();
  if ($string) {
    if (!$parent && $parent !== 0)  {
      $result = db_query_range(db_rewrite_sql("SELECT t.tid, t.name FROM {term_data} t INNER JOIN  {term_hierarchy} h ON t.tid = h.tid WHERE t.vid = %d AND t.weight = %d AND LOWER(t.name) LIKE LOWER('%%%s%%')", 't', 'tid'), $vid, $level, $string, 0, 12);
    } else {
      $result = db_query_range(db_rewrite_sql("SELECT t.tid, t.name FROM {term_data} t INNER JOIN  {term_hierarchy} h ON t.tid = h.tid WHERE t.vid = %d AND t.weight = %d AND h.parent = %d AND LOWER(t.name) LIKE LOWER('%%%s%%')", 't', 'tid'), $vid, $level, $parent, $string, 0, 12);
    }
    
    
    $matches = array();
    while ($term = db_fetch_object($result)) {
      $n = $term->name;
/*  // not special cases if not free tagging?
      // Commas and quotes in terms are special cases, so encode 'em.
      if (strpos($term->name, ',') !== FALSE || strpos($term->name, '"') !== FALSE) {
        $n = '"'. str_replace('"', '""', $term->name) .'"';
      }
*/
      $matches[$n] = check_plain($term->name);
    }
    print drupal_to_js($matches);
    exit();
  }
}
/**
 * Helper function for autocompletion restricted to children terms of a parent.
 *
 * Restricts autocomplete suggestions to terms directly under the parent term.
 * This is a new feature.  However this version disables free-tagging support.
 */
function place_taxonomy_smartcomplete($vid, $parent, $string = '') {
/*   free-tagging functionality commented out from taxonomy_autocomplete model
  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  // This regexp allows the following types of user input:
  // this, "somecmpany, llc", "and ""this"" w,o.rks", foo bar
  $regexp = '%(?:^|,\ *)("(?>[^"]*)(?>""[^"]* )*"|(?: [^",]*))%x';
  preg_match_all($regexp, $string, $matches);
  $array = $matches[1];

  // Fetch last tag
  $last_string = trim(array_pop($array));
  if ($last_string != '') {
*/
  if ($string != '') {
    $result = db_query_range(db_rewrite_sql("SELECT t.tid, t.name FROM {term_data} t INNER JOIN  {term_hierarchy} h ON t.tid = h.tid WHERE t.vid = %d AND h.parent = %d AND LOWER(t.name) LIKE LOWER('%%%s%%')", 't', 'tid'), $vid, $pid, $string, 0, 12);
/* more tagging-specific code
    $prefix = count($array) ? implode(', ', $array) .', ' : '';
*/
    $matches = array();
    while ($tag = db_fetch_object($result)) {
      $n = $tag->name;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (strpos($tag->name, ',') !== FALSE || strpos($tag->name, '"') !== FALSE) {
        $n = '"'. str_replace('"', '""', $tag->name) .'"';
      }
      $matches[$n] = check_plain($tag->name);  // cut '$prefix . ' from $matches[]
    }
    print drupal_to_js($matches);
    exit();
  }
}
