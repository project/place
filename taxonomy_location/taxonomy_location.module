<?php

/**
* @file
* taxonomy_location.module provides an association between taxonomy terms and
* locations.
*
* Written by Benjamin Melan�on of Agaric Design Collective, October 2007.
* http://AgaricDesign.com
*/

// include admin functions @TODO prevent from loading unless it needs to
// at end of file for now
// include_once './'. drupal_get_path('module', 'taxonomy_location') .'/taxonomy_location_admin.inc';

/**
 * Provides the location associated with the given term id as an object. 
 *
 * One might expect this to be called taxonomy_location_term or 
 * taxonomy_location_term_get or even place_taxonomy_term_get
 * but it's not.  But that might help me find it!
 *
 * @param  int $tid
 *    The term id.
 * 
 * @return array
 *    An array with basic location information (latitude, 
 *    longitude, radius) as well as term name.
 */
function taxonomy_location_get($tid) {
  static $location = array();
  // do lookup, return full display path
  if (!$location[$tid]) {
    $location[$tid] = _taxonomy_location_get($tid);
/*  @TODO ben-agaric maybe later
    else if (variable_get('taxonomy_image_recursive', 0)) {
      // walk up the taxonomy hierarchy and look for an image
      $orig = $tid;
      while ($parent = db_fetch_object(db_query('SELECT t.tid FROM {term_hierarchy} h, {term_data} t WHERE h.parent = t.tid AND h.tid = %d ORDER BY weight, name', $tid))) {
        $tid = $parent->tid;
        if ($image[$tid]->url) {
          // we have already loaded this image from the database
          $image[$orig] = $image[$tid];
          break;
        } 
        else if ($image[$tid] = db_fetch_object(db_query('SELECT i.path, d.name FROM {term_image} i INNER JOIN {term_data} d WHERE i.tid = d.tid AND i.tid = %d', $tid))) {
          // we found a parent with a configured image, use it
          $image[$tid]->url = file_create_url($image[$tid]->path);
          $image[$orig] = $image[$tid];
          break;
        }
      }
    }
*/
  }
  if ($location[$tid]) {    
    return $location[$tid];
  }
  return '';
}

/**
 * Implementation of hook_perm()
 */
function taxonomy_location_perm() {
  return array ('access taxonomy location', 'administer taxonomy location');
}

/**
 * Implementation of hook_help()
 */
function taxonomy_location_help($section = '') {
  switch ($section) {
    case 'admin/content/taxonomy/location':
      return t('The taxonomy_location module allows the association of basic location information, geographical coordinates, with category terms.  Once defined, this association allows Drupal to use several  with site content.   
      
      To learn more about how to create vocabularies and terms, review the <a href="%taxonomy-help">taxonomy help page</a>.', array('%taxonomy-help' => url('admin/help/taxonomy')));
    case 'admin/help#taxonomy_location':
      return t('
      <h3>Introduction</h3>
      <p>The taxonomy_location module allows the association of simple geographical coordinate location information with category terms.      <p>The taxonomy_location module requires that the taxonomy module also be enabled.</p>
      <h3>Configuration</h3>
      <h4>Populating locations</h4>
      <p>Terms with locations can be uploaded at \'administer >> categories >> locations\'.  On that page you will find tables containing all your vocabularies and terms.  Next to each term is a link titled \'edit location\' which you can click to update location information (latitude, longitude, radius/zoom) for that term.</p>
      <h4>Permissions</h4>
      <p>For your users to be able to view the geographical coordinates in your location-aware vocabulary, you will need to give them the necessary permissions.  Only users with the \'access taxonomy location\' permission will see location information and maps derived from them (in theory).</p>
      <p>A third permission, \'administer taxonomy location\', controls which users are allowed to configure the taxonomy location module.</p>
      <h4>NOT IMPLEMENTED: Recursive location display</h4>
      <p>Taxonomy is a very powerful tool.  One of its features is providing the ability to create hierarchical vocabularies, with which you can build a tree of terms.  It is possible that an entire tree of terms, or even just a branch of terms, are all about a similar subject and should all be associated with the same location.  By going to \'Administer >> Settings >> Taxonomy location\', you can enable \'Recursive location \'.  With this option enabled, you only need to configure a location for the parent term, and all children will automatically inherit the same image (unless they are manually configured to display an alternative image).</p>
      <h3>Displaying locations</h3>
      <p>The good displays will be handled by mapping modules.</p>
      <p>Alternately, to display locations from your theme, you will have to modify the theme to make a call to theme(\'taxonomy_location_display\', $tid).  For example, from your theme\'s \'_node\' template you might do the following:
<pre>
  foreach (taxonomy_node_get_terms($node->nid) as $term) {
    print theme(\'taxonomy_location_display\', $term->tid);
  }
</pre>
');
  }
}

function taxonomy_location_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'admin/content/taxonomy/location', 'title' => t('Category Location'),
      'callback' => 'taxonomy_location_admin',
      'access' => user_access('administer taxonomy location'),
      'type' => MENU_LOCAL_TASK);
  }

  return $items;
}

/**
 * Implementation of hook_form_alter().
 *
 * An important hook for taxonomy_location.
 * First, to make vocabularies location-aware by making it an option
 * on the create and edit vocabulary forms.
 */
function taxonomy_location_form_alter($form_id, &$form) {
  // these if statements should be reworked as a switch statement?
  if ($form_id == 'taxonomy_form_term' && taxonomy_location_vid($form['vid']['#value'])) {
    $insert = _taxonomy_location_term_edit($form);
    $pos = array_search('submit', array_keys($form));
    $form = array_merge(array_slice($form, 0, $pos), $insert, array_slice($form, $pos));
    $form['submit']['#weight'] = 10;
    $form['delete']['#weight'] = 10;
  }
  elseif ($form_id == 'taxonomy_form_vocabulary') {
    $insert = _taxonomy_location_vocab_settings($form);
    $pos = array_search('submit', array_keys($form));
    $form = array_merge(array_slice($form, 0, $pos), $insert, array_slice($form, $pos));
  }
  elseif ($form_id == 'taxonomy_vocabulary_confirm_delete') {
    $form['taxonomy_location_delete'] = array(
      '#type' => 'markup',
      '#value' => '<div>' . t('<em>Deleting the location-aware vocabulary</em> @name <em>will also delete all location information associated with it.</em>', array('@name' => $form['name']['#value'])) . '</div>',
      '#weight' => 10,
    );
    $form['actions']['#weight'] = 11;
  }
}


/**
 * Implementation of hook_taxonomy()
 */
function taxonomy_location_taxonomy($op, $type, $array) {
  switch ($type) {
    case 'term':
      switch ($op) {
/* does not appear to be a validate op, have to set custom ones */
        case 'insert':
        case 'update':
          // don't do anything if all blank-- it's evil taxonomy manager
          if (isset($array['taxonomy_location_longitude'])) {
            $term_location = array(
              'tid' => $array['tid'],
              'latitude' => $array['taxonomy_location_latitude'],
              'longitude' => $array['taxonomy_location_longitude'],
              'radius' => $array['taxonomy_location_radius'],
            );
            taxonomy_location_save($term_location);           
          }
          break;
        case 'delete':
          taxonomy_location_delete($array['tid']);
          break;
      }
    
      break;
    case 'vocabulary':
      // if it either was or is about to be location aware
      if ($array['taxonomy_location_enabled'] || $array['taxonomy_location_enabled_orig']) {
        // initial threshold key, making 'reset' feature possible.  Flip to negative if disabled, so reset would stay available.
        switch ($op) {
          case 'update':
            if (!db_fetch_object(db_query('SELECT vid FROM {taxonomy_location_vocabulary} WHERE vid = %d', $array['vid']))) {
              $op = 'insert';
            }
            taxonomy_location_save_vocabulary($array, $op);
            break;
          case 'insert':
            taxonomy_location_save_vocabulary($array, $op);
            break;
          case 'delete':
            _taxonomy_location_del_vocabulary($array['vid']);
          break;
       }  // end switch on op
      break;  // end case 'vocabulary'
    }  // end if cmt_vocabulary set
  }  // end switch on type
}


/**
 *  List all node types affected by any location-aware vocabulary.
 */
function taxonomy_location_node_types($names = NULL) {
  static $taxonomy_location_node_types, $taxonomy_location_node_types_names;
  if (!$taxonomy_location_node_types) {
    $result = db_query('SELECT DISTINCT(v.type) AS type FROM {taxonomy_location_vocabulary} t LEFT JOIN {vocabulary_node_types} v ON t.vid = v.vid WHERE t.enabled > 0');
    $taxonomy_location_node_types = array();
    while ($data = db_fetch_object($result)) {
      $taxonomy_location_node_types[] = $data->type;
    }   
  }
  if ($names == 'names' && !$taxonomy_location_node_types_names) {
// @todo could replace with place_taxonomy_node_types_names
    $all_node_types = node_get_types('names');
    foreach ($all_node_types AS $node_type => $node_type_name) {
      if (!in_array($node_type, $taxonomy_location_node_types))  unset($all_node_types[$node_type]);
    }
    $taxonomy_location_node_types_names = $all_node_types;
    return $taxonomy_location_node_types_names;
  }
  return $taxonomy_location_node_types;
}


/**
 * Additions to taxonomy term edit form.
 */
function _taxonomy_location_term_edit($form) {
  $term = taxonomy_location_get($form['tid']['#value']);
  $tlte['taxonomy_location_intro'] = array(
    '#type' => 'markup',
    '#value' => '<p>' . t('This is a location-aware taxonomy term.') . '</p>',
    '#weight' => -30,
  );
  $tlte['taxonomy_location_code'] = array(
    '#type' => 'textfield',
    '#maxlength' => 4,
    '#size' => 4,
    '#title' => t('Alphanumeric code'),
    '#default_value' => $term['code'],
    '#weight' => 1,
  );
  $tlte['taxonomy_location_latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#size' => 12,
    '#default_value' => $term['latitude'],
    '#weight' => 5,
  );
  $tlte['taxonomy_location_longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#size' => 12,
    '#default_value' => $term['longitude'],
    '#weight' => 5,
  );
  $tlte['taxonomy_location_radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Google map zoom'),
    '#size' => 6,
    '#default_value' => $term['radius'],
    '#weight' => 6,
  );  
  return $tlte;  // return taxonomy location addition to term edit form
}

/**
 * Administration form
 *
 * Separate function in case we want CMT-style separate admin pages.
 */
function _taxonomy_location_vocab_settings($form) {
  if(taxonomy_location_vid($form['vid'])) {
    $vocab = taxonomy_location_get_vocabulary($form['vid']);
  }
  else {
    $vocab = array(
      'enabled' => 0,
    );
  }
  $tlvf['taxonomy_location_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Locations'),
    '#default_value' => $vocab['enabled'],
    '#options' => array(
      0 => t('No taxonomy locations'), 
      1 => t('Enable taxonomy locations'), 
//      2=> t('Enable and recurse or inherit locations')
    ),
    '#description' => t('A location-aware vocabulary can store basic geographical information with its terms.'),
//  When enabled, taxonomy tolation will recursively search for a location to associate with a term, starting with the passed in term, then trying the term\'s parents.  This functionality is only useful if you have defined hierarchical taxonomies, and multiple terms within a tree will share the same location.
  );
  $tlvf['taxonomy_location_enabled_orig'] = array(
    '#type' => 'value',
    '#value' => $vocab['enabled'],
  );
  return $tlvf;  // return taxonomy location additions to vocabulary form
}


/**
 * Return taxonomy location info for a vocabulary.  Results statically cached.
 *
 * @param $vid
 *   The vocabulary's ID
 *
 * @return array
 *   The vocabulary sub-object with all taxonomy_location metadata.
 *    - latitude
 *    - longitude
 *    - radius
 *    - code
 */
function taxonomy_location_get_vocabulary($vid) {
  static $taxonomy_location_vocabularies = array();
  if (!array_key_exists($vid, $taxonomy_location_vocabularies)) {
    $result = db_query('SELECT tl.* FROM {taxonomy_location_vocabulary} tl WHERE tl.vid = %d', $vid);
    $taxonomy_location_vocabularies[$vid] = (array)db_fetch_object($result);
  }
  return $taxonomy_location_vocabularies[$vid];
}

/**
 * Return an array of all vocabulary objects for location-aware
 * vocabularies.
 *
 * @param $enabled
 *   Return only those CMT vocabularies which are either 'enabled' or 
 *   'disabled'.  Default both.
 */
function taxonomy_location_get_vocabularies($enabled = 'both') {
  $where = "";
  if ($enabled == 'enabled') $where = "WHERE lv.enabled > 0 ";
  elseif ($enabled == 'disabled') $where = "WHERE lv.cmt_enabled = 0 ";
  $c = db_query(db_rewrite_sql("SELECT v.*, n.type FROM {vocabulary} v INNER JOIN {taxonomy_location_vocabulary} lv ON v.vid = lv.vid LEFT JOIN {vocabulary_node_types} n ON v.vid = n.vid $where ORDER BY v.weight, v.name", 'v', 'vid'));
  // below identical to taxonomy_get_vocabularies
  // couldn't we - and taxonomy core, and cmt - just use taxonomy_get_vocabulary in a loop?
  $vocabularies = array();
  $node_types = array();
  while ($voc = db_fetch_object($c)) {
    $node_types[$voc->vid][] = $voc->type;
    unset($voc->type);
    $voc->nodes = $node_types[$voc->vid];
    $vocabularies[$voc->vid] = $voc;
  }
  return $vocabularies;
}

/**
 * Taxonomy location overview.
 */
function taxonomy_location_overview() {
  $output = '';
  if (variable_get('taxonomy_location_recursive', 0)) {
    $output .= '<h4>'. t('Recursively displaying images.') .'</h4>';
  }

  $header = array(t('name'), t('node types'), t('image'));

  $vocabularies = taxonomy_location_get_vocabularies();

  foreach ($vocabularies as $vocabulary) {
    $types = array();
    $rows = array();
    foreach(explode(',', $vocabulary->nodes) as $type) {
      $types[] = node_invoke($type, 'node_name');
    }
    $rows[] = array($vocabulary->name, array('data' => implode(', ', $types), 'align' => 'center'), '');

    $tree = taxonomy_get_tree($vocabulary->vid);
    if ($tree) {
      foreach ($tree as $term) {
        $data =  str_repeat('--', $term->depth) .' '. $term->name .' ('. ( _taxonomy_location_exists($term->tid) ? l(t('edit location'), "admin/content/taxonomy/location/edit/$term->tid") : l(t('input location'), "admin/content/taxonomy/location/add/$term->tid") ) .')<br />';
        $location = taxonomy_location_display($term->tid) ? taxonomy_location_display($term->tid) : '';
        $rows[] = array(array('data' => $data, 'colspan' => 2), $image);
      }
    }  
    
    $output .= theme('table', $header, $rows);
  }

  return $output;
}

function _taxonomy_location_get($tid) {
  return (array)db_fetch_object(db_query('SELECT l.latitude, l.longitude, l.radius, l.code, d.name FROM {term_location} l INNER JOIN {term_data} d WHERE l.tid = d.tid AND d.tid = %d', $tid));
}

/**
 * Save a taxonomy location.
 *
 * Takes a term_location object.
 */
function taxonomy_location_save($term_location) {
  $tid = $term_location['tid'];
  /*
  Drupal doesn't seem to be able to set a column to NULL
  if (!$term_location['longitude'] && $term_location['longitude'] !== 0) {
    $term_location['longitude'] = NULL;
  }
  $term_location['latitude'] = NULL;
  */
  if (_taxonomy_location_exists($tid)) {
    // update
    db_query("UPDATE {term_location} SET latitude = %f, longitude = %f, radius = %d, code = '%s' WHERE tid = %d", $term_location['latitude'], $term_location['longitude'], $term_location['radius'], $term_location['code'], $tid);
  }
  else {
    // new location data
    db_query("INSERT INTO {term_location} (tid, latitude, longitude, radius, code) VALUES (%d, %f, %f, %d, '%s')", $tid, $term_location['latitude'], $term_location['longitude'], $term_location['radius'], $term_location['code']);
  }
  // cache_clear_all();  // not necessary?  could be more specific?
  // http://api.drupal.org/api/function/cache_clear_all/5
  return;
}


/**
 * Delete term location information for the term with the given tid.
 */
function taxonomy_location_delete($tid) {
  db_query('DELETE FROM {term_location} WHERE tid = %d', $tid);
  // cache_clear_all();  // is this necessary?  
}

function _taxonomy_location_exists($tid) {
  if (db_fetch_object(db_query('SELECT latitude FROM {term_location} WHERE tid = %d', $tid))) {
    return TRUE;
  }
  return FALSE;
}


/**
 * Return true if a vocabulary ID belongs to a location-aware vocabulary.
 *
 * Identical to cmt_is_vid_cmt()
 * @TODO: Needs to be optimized
 */
function taxonomy_location_vid($vid) {
  return in_array($vid, array_keys(taxonomy_location_get_vocabularies()));
}


// split into admin section when file encoding or line endings or whatever the damn problem is gets figured out or starts working

function taxonomy_location_save_vocabulary($edit, $op = 'insert') {
  switch ($op) {
    case 'update':
      $enabled = ($edit['taxonomy_location_enabled']) ? $edit['taxonomy_location_enabled'] : $edit['taxonomy_location_enabled_orig'] * -1;
      if (db_query("UPDATE {taxonomy_location_vocabulary} tl SET tl.enabled = %d WHERE tl.vid = %d", $enabled, $edit['vid'])) {        
        return SAVED_UPDATED;     
      }
      break;
    case 'insert':  
      // $edit['vid'] should be available from taxonomy_save_vocabulary, which had it by reference
      if (db_query("INSERT INTO {taxonomy_location_vocabulary} (vid, enabled) VALUES (%d, %d)", $edit['vid'], $edit['taxonomy_location_enabled'])) {
        return SAVED_NEW;
      }
      break;
/* @TODO - sort out the appropriate
              drupal_set_message(t('The vocabulary %name is now location aware.', array('%name' => $array['name'])));
            }
            else  drupal_set_message(t('Making this vocabulary location-aware failed somehow.'), 'error');

              drupal_set_message(t('Your new vocabulary is location aware.'));
            }
            else drupal_set_message(t('Making your new vocabulary location-aware failed somehow.'), 'error');
*/            
  }
}

/**
 * Delete a vocabulary.
 *
 * @param $vid
 *   A vocabulary ID.
 * @return
 *   Constant indicating items were deleted.
 */
function _taxonomy_location_del_vocabulary($vid) {
  db_query('DELETE FROM {taxonomy_location_vocabulary} WHERE vid = %d', $vid);
  // @TODO: this better be called *before* taxonomy does it's work...
  // it's not.  Damn you, drupal.
  // I suppose the query could be rewritten to delete every term from
  // taxonomy_location_term_location that's not in term_data
  // that can be done any time.
  $result = db_query('SELECT tid FROM {term_data} WHERE vid = %d', $vid);
  while ($term = db_fetch_object($result)) {
    db_query('DELETE FROM {taxonomy_location_term_location} WHERE tid = %d', $tid);
  }
  return SAVED_DELETED;
}