Overview:
--------
The taxonomy_location module allows site administrators to associate locations
with taxonomy terms.  With the association created, an admin can then make
a call to 'theme_taxonomy_location_display()' from their theme or other PHP code
to display the appropriate location.

The module allows the admin to create a one-to-one term-to-location relationship.
With (planned) location display recursion, it is also possible to create a many-to-one
relationship.  This is useful if creating taxonomy hierarchies in which an
entire tree of terms will use the same location.  With recursion enabled, you
only need to associate an location with the tree parent, and all children will
automatically inherit the same location.


Features:
--------
 - location editing happens within existing admin/content/taxonomy/edit/term/[tid]
 - Allows one-to-one term-to-location relationships
 - Allows many-to-one term-to-location relationships
 - Small module with specific, focused functionality

Installation and configuration:
------------------------------
Please refer to the INSTALL file for complete installation and 
configuration instructions.

Comments:
--------
I want to try very hard to help all location-related modules play well with one another and share code and functionality when possible.  Contact me with ways I should change this module toward that end, and related modules to extend it, and in general how location modules could potentially and should hook into one another.

Requires:
--------
 - Drupal 5.0
 - enabled taxonomy.module

Credits:
-------
 - Written and maintained by Benjamin Melan�on of Agaric Design Collective
   http://agaricdesign.com/